
package ldapsync;

/**
 * A representation of a student 
 * @author sf659
 */
public class Student extends User {

    private Year currentYear = Year.UNKNOWN;
    private String awardCode = null;

    /**
     * Enum to describe valid student years of study 
     */
    public enum Year {

        /**
         * Defines foundation year
         */
        FOUNDATION,
        /**
         * Defines first year
         */
        FIRST,
        /**
         * Defines second year
         */
        SECOND,
        /**
         * Defines third year
         */
        THIRD,
        /**
         * Defines fourth year
         */
        FOURTH,
        /**
         * Defines postgrad
         */
        POSTGRAD,
        /**
         * Defines an unknown year
         */
        UNKNOWN
    }

    /**
     * Constructor for Student 
     */
    public Student() {
    }

   
    public void setAwardCode(String awardCode){
        this.awardCode = awardCode;
        
    }
    
    public String getAwardCode(){
        return awardCode;
    }
    
    /**
     * The course year is set as a string and can even include letters,
     * post grads are listed as W and some years are 1B / 2B etc
     * @param year String representation of the year
     */
    public void setYear(String year) {

   
        if (year.matches("0")) {
            currentYear = Year.FOUNDATION;
        } else if (year.matches("1[A-Z]*")) {
            currentYear = Year.FIRST;
        } else if (year.matches("2[A-Z]*")) {
            currentYear = Year.SECOND;
        } else if (year.matches("3[A-Z]*")) {
            currentYear = Year.THIRD;
        } else if (year.matches("4[A-Z]*")) {
            currentYear = Year.FOURTH;
        } else if (year.matches("W")) {
            currentYear = Year.POSTGRAD;
        } else {
            currentYear = Year.UNKNOWN;
        }
        
        // Force users who have awardcode as PHD as postgrads,
        // this is because some postgrads will have a phd year as a numeric
        // value
         if(awardCode.equals("PHD") || awardCode.equals("MSCR")){
            currentYear = Year.POSTGRAD;
        }

    }

    /**
     * Return a current students year of study
     * @return The year of study 
     */
    public Year getYear() {
        return currentYear;
    }

    /**
     * Return a current year of study based on the Distinguished Name
     * @param dn The distinguished name
     * @return The current year of study
     */
    public Year getYear(String dn) {

        if (dn.matches(".*ou=foundation.*")) {
            return Year.FOUNDATION;
        } else if (dn.matches(".*ou=firstyear.*")) {
            return Year.FIRST;
        } else if (dn.matches(".*ou=secondyear.*")) {
            return Year.SECOND;
        } else if (dn.matches(".*ou=thirdyear.*")) {
            return Year.THIRD;
        } else if (dn.matches(".*ou=fourthyear.*")) {
            return Year.FOURTH;
        } else if (dn.matches(".*ou=postgrad.*")) {
            return Year.POSTGRAD;
        } else {
            return Year.UNKNOWN;
        }

    }
    
    /**
     * Returns a current students correct organisational unit based on their year
     * @param year The current year of student
     * @return The organisational unit that year belongs to
     */
    public String getOU(Year year){
        
        switch(year){
            case FOUNDATION:
                return "ou=foundation";
            case FIRST:
                return "ou=firstyear";
            case SECOND:
                return "ou=secondyear";
            case THIRD:
                return "ou=thirdyear";
            case FOURTH:
                return "ou=fourthyear";
            case POSTGRAD:
                return "ou=postgrad";
        }
     
        return "unknown";
    }
    
    /**
     * Returns an account group based on the current year of study for a student
     * @param year The current year of student
     * @return String representation of the current account group
     */
    public String getAccountGroup(Year year){
          switch(year){
            case FOUNDATION:
                return "foundation";
            case FIRST:
                return "firstyear";
            case SECOND:
                return "secondyear";
            case THIRD:
                return "thirdyear";
            case FOURTH:
                return "fourthyear";
            case POSTGRAD:
                return "postgrad";
        }
     
        return "unknown";
    }
        
 }

