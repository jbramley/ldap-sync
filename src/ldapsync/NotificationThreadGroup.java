
package ldapsync;

import java.util.HashSet;
import java.util.List;

/**
 * A  NotificationThreadGroup is a collection of {@link NotificationThread} that are to be scheduled 
 * together and in a particular order. </p>
 * 
 * We can pass a group of {@link NotificationThread} as a {@link List} these NotificationThreads will then run concurrently once they have all completed
 * the {@link NotificationThread} defined as runComplete will then be scheduled to run. This is required as we may have a {@link NotificationThread}
 * that can't run till a group of {@link NotificationThread} has finished or that may cause a deadlock is they were to run concurrently.
 * @author sf659
 */
public class NotificationThreadGroup {
    
    
    private List<NotificationThread> notificationGroup;
    private NotificationThread runComplete;
    private HashSet complete;
    private boolean onetimeRun;
    private boolean runCompleteExists = false; // Defines if there is a valid run complete thread to be executed
    
    /**
     * Creates a new NotificationThreadGroup
     * @param notificationGroup A collection of {@link NotificationThread} that can run concurrently 
     * @param runComplete A  {@link NotificationThread} that can only run after all the NotificationThreads in the notificationGroup have finished running
     * @param onetimeRun A boolean to define is we should keep scheduling this NotificationThreadGroup or only run the {@link NotificationThread} in the group once.
     */
    public NotificationThreadGroup(List<NotificationThread> notificationGroup, NotificationThread runComplete, boolean onetimeRun){
        
        runCompleteExists = true;
        this.notificationGroup = notificationGroup;
        this.runComplete = runComplete;
        complete = new HashSet(notificationGroup.size());
        this.onetimeRun = onetimeRun;
        setShutdownHooks();
        
    }
    
    public NotificationThreadGroup(List<NotificationThread> notificationGroup, boolean  onetimeRun){
        this.notificationGroup = notificationGroup;
        complete = new HashSet(notificationGroup.size());
        this.onetimeRun = onetimeRun;
        setShutdownHooks();
        
    }
    
    /**
     * Calls all the NotificationThreads within this groups setShutdownHooks methods
     * thus ensuring all NotificationThreads will have activated their shutdown hooks
     */
    private void setShutdownHooks(){
        
        for(NotificationThread nt : notificationGroup){
            nt.setshutdownHook();  
        }
        //This happens if there is no job to be schedule after the initial group
        if(runCompleteExists){
          runComplete.setshutdownHook();
        }
    }
    
    /**
     * Checks if the current group is scheduled to run more than once
     * @return true if the current group is scheduled to run more than once
     */
    public boolean isOneTimeRun(){
        return onetimeRun;
    }
    
    
    /**
     * Checks to see if a {@link NotificationThread} is contain within this group
     * @param notificationThread The {@link NotificationThread} that is been checked for inclusion
     * @return true if the group contains the notificationThread
     */
    public boolean  containsNotificationThread(NotificationThread notificationThread){
           if (notificationGroup.contains(notificationThread)){
               return true;
           }
        
        return false;
        
    }
    
    /**
     * Marks the {@link NotificationThread} as complete (its doRun() method has ended)
     * @param thread the {@link NotificationThread} is be marked as complete
     */
    public void threadComplete(NotificationThread thread){
        
        complete.add(thread);
       
    }
    
    /**
     * Returns the list of NotificationThreads in this group
     * @return a list of NotificationThreads
     */
    public List<NotificationThread> returnNotificationThreadGroup(){
        return notificationGroup;
    }
    
    /**
     * Adds a {@link TaskListener} to each {@link NotificationThread} within this
     * {@link NotificationThreadGroup}
     * @param listner the {@link TaskListener} to add
     */
    public void addNotificationThreadListener(TaskListener listner){
        
      for (NotificationThread thread : notificationGroup){
          thread.addListener(listner);
      }
      if(runCompleteExists){
         runComplete.addListener(listner);
      }
    }
    
    /**
     * Returns the current group size
     * @return group size
     */
    public int getNotificationThreadGroupSize(){
        return (notificationGroup.size() + 1);
    }
    
    /**
     * Returns the {@link NotificationThread} designated to run after
     * all other {@link NotificationThread}s have finished running
     * @return
     */
    public NotificationThread returnRunCompleteThread(){
        return runComplete;
    }
    
    /**
     * Returns true if this notification thread group contains a {@link NotificationThread}
     * to run on completion. 
     * @return true is such a {@link NotificationThread} exists
     */
    public boolean doesrunCompleteExists(){
        return runCompleteExists;
    }
    
    /**
     * Have all the current {@link NotificationThread}s in this group finished.
     * @return true if all the {@link NotificationThread}s in this group have finished
     */
    public boolean allComplete(){
        if (complete.size() == notificationGroup.size()){
                complete.clear();
                return true;
                
            
        }
        return false;
    }
    
    
    
}
