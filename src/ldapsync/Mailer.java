package ldapsync;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/**
 * The mailer class allows us to send email notifications
 * @author sf659
 */
public class Mailer {

    private String mailServer = "npg.york.ac.uk";
    private String mailPort = "25";
    private String fromAddress = "phys19@york.ac.uk";
    private String toAddress = "phys-support@npg.york.ac.uk";
    private Properties properties = new Properties();
    private Session mailSession;
    private Message simpleMessage;
    
    /**
     * Send a message with a message and a title
     * @param message body of message to send
     * @param title title of message to send
     */
    public Mailer(String message, String title){
       sendMessage(message, title);
        
        
    }
    
    /**
     * Send a message with an exception and a title
     * @param exception exception to send in the body of the message
     * @param title title of message to send
     */
    public Mailer(Exception exception, String title) {
 
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            exception.printStackTrace(pw);
            sendMessage(sw.toString(), title);

    }
    
    /**
     * Provides the actualy implementation to send our email
     * @param message body of message to send
     * @param title title of message to send
     */
    private void sendMessage(String message, String title){
        try {
            properties.put("mail.smtp.host", mailServer);
            properties.put("mail.smtp.port", mailPort);
            mailSession = Session.getDefaultInstance(properties);
            simpleMessage = new MimeMessage(mailSession);
            
            simpleMessage.setFrom(new InternetAddress(fromAddress));
            simpleMessage.setRecipient(Message.RecipientType.TO,new InternetAddress(toAddress));
            simpleMessage.setSubject(title);
            simpleMessage.setText(message);
            
             Transport.send(simpleMessage);
            
            
        } catch (MessagingException ex) {
            Logger.getLogger(Mailer.class.getName()).log(Level.SEVERE, null, ex);
        }
      
    }
}
