package ldapsync;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import oracle.ucp.jdbc.PoolDataSource;
import oracle.ucp.jdbc.PoolDataSourceFactory;
import oracle.ucp.jdbc.ValidConnection;

/**
 * The DatawarehouseReader class interacts with the Central Data warehouse, which is an oracle database,</p>
 * http://www.york.ac.uk/about/departments/support-and-admin/student-systems/systems/dwh/</p>
 * <b>Current List of tables: http://www.york.ac.uk/admin/yimshelp/sturecs/sitsdwh_index.htm</b></p>
 * The search query currently preformed is </p>
 * 
 * select * from scad where dpt_code='0004' or dpt_code='0005' and ROU_CODE like 'U%MAT%PHY%'</p>
 * 
 * The Physics Department code is 0004</p>
 * The Maths Department code is 0005</p>
 * 
 * Students doing maths and physics are listed with their primary department as maths, this doesn't
 * appear to happen in any other cases.</p>
 * </p>
 * A valid username / password is required to access the data warehouse see </p>
 * http://www.york.ac.uk/univ/mis/cfm/systemaccess/sysacc_start.cfm</p>
 * for registration.</p>
 * </p>
 * 
 * 
 * @author sf659
 */
public class DatawarehouseReader extends NotificationThread {

    private Connection connection = null;
    /** Column defining IT Services username in SCAD TABLE*/
    private final String itservicesnoValue = "STU_ITNO";
    /** Column defining current year of study in SCAD TABLE*/
    private final String yearOfStudy = "SCE_BLOK"; //Year student is currently in 
    private HashMap<String, Student> students = null;
    private final String query = "select * from scad where dpt_code='0004' or dpt_code='0005' and ROU_CODE like 'U%MAT%PHY%'";
    private final String awardcode = "AWD_CODE"; // used to distinguish postgrads
    private String username;
    private String password;
    private String host;
    private String port;
    private String servicename;
    private PoolDataSource dataWareHouse;
    private PreparedStatement selectPhysStudents;

    /**
     * Creates a new DatawarehouseReader using the provided credentials.
     * @param username Username used to connect to the data warehouse
     * @param password Password for username used to connect  to the data warehouse
     * @param host Host where the oracle service resides
     * @param port Post on which the oracle service run 
     * @param servicename Servicename for the data warehouse
     * @param students A empty hashmap that will be populated with Student objects once the query has run
     */
    public DatawarehouseReader(String username, String password, String host, String port,
        String servicename, HashMap students) {

        this.username = username;
        this.password = password;
        this.host = host;
        this.port = port;
        this.servicename = servicename;
        this.students = students;
        dataWareHouse = PoolDataSourceFactory.getPoolDataSource();
        
    }

    /**
     * Preforms the connection to the OracleDataSource in this case the data warehouse
     * @throws SQLException 
     */
    private void connect() throws SQLException {
        //http://docs.oracle.com/cd/E11882_01/java.112/e12265/connect.htm
        dataWareHouse.setConnectionFactoryClassName("oracle.jdbc.pool.OracleDataSource");
        dataWareHouse.setUser(username);
        dataWareHouse.setPassword(password);
        dataWareHouse.setURL("jdbc:oracle:thin:@//"+host+":"+port+"/"+servicename);
        dataWareHouse.setMaxPoolSize(1);

        connection = dataWareHouse.getConnection();
        
        Logger.getLogger(DatawarehouseReader.class.getName()).log(Level.INFO, "Connection to data warehouse sucessfull\n");
    }

    /**
     * Disconnects the connection object from the OracleDataSource
     * @throws SQLException 
     */
    private void disconnect() throws SQLException {
      
        selectPhysStudents.close();
        ((ValidConnection) connection).setInvalid();
        connection.close();
        connection = null;

    }

    /**
     * Preforms the actual look up of the students data, populating the HashMap students with 
     * student objects.
     * @throws SQLException
     * @throws NullPointerException 
     */
    private void searchPhysicsStudents() throws SQLException, NullPointerException {

        connect();

        selectPhysStudents = connection.prepareStatement(query);
        ResultSet studentResults = selectPhysStudents.executeQuery();

        int noStudents = 0;
        while (studentResults.next()) {

            String itserviceno = studentResults.getString(itservicesnoValue);
            String courseYear = studentResults.getString(yearOfStudy);
            String award = studentResults.getString(awardcode);
            

            Student student = new Student();
            student.setUsername(itserviceno);
            student.setAwardCode(award);
            student.setYear(courseYear);
            
            noStudents = noStudents + 1;
            students.put(student.getUsername(), student);

            //If logger level is fine we will show every user found in the output
            Logger.getLogger(DatawarehouseReader.class.getName()).log(Level.FINE, "User {0} found", itserviceno);

        }

        Logger.getLogger(DatawarehouseReader.class.getName()).log(Level.INFO, "{0} students found", noStudents);
        //disconnect once this search is over
        disconnect();

    }

    /**
     * The NotificationThread run method, when this thread is scheduled to run this method is executed
     */
    @Override
    public void doRun() {
        try {
            searchPhysicsStudents();
        } catch (NullPointerException ex) {
            exceptionOccured(ex, "Generating Hash Code for user failed, username not found");

        } catch (SQLException ex) {
            exceptionOccured(ex, "SQL Error searching for student");
        } 
    }

    /**
     * Ensures any open connections are closed before an exit
     */
    @Override
    public void setshutdownHook() {
        //Shutdown Hook
        Runtime.getRuntime().addShutdownHook(new Thread() {

            @Override
            public void run() {
                try {
                    if (connection != null) {
                        ((ValidConnection) connection).setInvalid();
                        connection.close();
                        
                    }
                    if (selectPhysStudents != null) {
                        selectPhysStudents.close();
                    }
                    Logger.getLogger(DatawarehouseReader.class.getName()).log(Level.FINE, "DatawarehouseReader shutdown\n");

                } catch (SQLException ex) {
                    Logger.getLogger(DatawarehouseReader.class.getName()).log(Level.SEVERE, "Shutdown hook failed, Datawarehouse reader\n", ex);
                    System.exit(-1);
                }
            }
        });
    }
}
