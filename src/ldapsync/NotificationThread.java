package ldapsync;

import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;

/**
 * NotificationThread is an implementation that allows our threads to notify a listener
 * once they have completed their doRun() method.
 * @author sf659
 */
public abstract class NotificationThread implements Runnable {

    private final Set<TaskListener> listeners = new CopyOnWriteArraySet<TaskListener>();

    /**
     * Adds a listener to the NotificationThread.
     * @param listener the TaskListener to add
     */
    public final void addListener(final TaskListener listener) {
        listeners.add(listener);
    }

    /**
     * Removes the listener from the NotificationThread.
     * @param listener the TaskListner to remove
     */
    public final void removeListener(final TaskListener listener) {
        listeners.remove(listener);
    }

    /**
     * Notififies all listeners that the NotificationThread has completed.
     */
    private void notifyListeners() {
        for (TaskListener listener : listeners) {
            listener.threadComplete(this);
        }
    }

    /**
     * Notifies all listeners that an exception in the NotificationThread has occured.
     * @param ex the exception that has occured
     */
    public void exceptionOccured(Exception ex) {
        for (TaskListener listener : listeners) {

            listener.exceptionOccured(this, ex);
        }
    }
    
    /**
     * Notifies all listeners that an exception in the NotificationThread has occured.
     * @param ex the exception that has occured
     * @param msg a message to pass along with the exception
     */
    public void exceptionOccured(Exception ex, String msg){
        for (TaskListener listener : listeners) {

            listener.exceptionOccured(this, ex, msg);
        }
    }
    
    /**
     * Notifies all listeners that an non fatal exception in the NotificationThread has occured,
     * this is usualy where an modification operation could not be preformed and can be treated
     * to start with as non fatal.
     * @param ex the exception that occurec
     */
    public void nonFatalExceptionOccured(Exception ex){
        for (TaskListener listener : listeners) {

            listener.nonFatalExceptionOccured(this, ex);
        }
    }
    
    /**
     * Informs all tasklisteners to schedule events
     * @param notificationGroup A group of NotificationThreads to be scheduled
     */
    public void scheduleEvent(NotificationThreadGroup notificationGroup){
        for(TaskListener listener : listeners){
            listener.scheduleEvents(notificationGroup);
          
        }
    }
    
     /**
     * All notification threads should have a shutdown hook set to 
     * close open connections
     */
    public abstract void setshutdownHook();
      
    /**
     * All notification threads have a doRun method where the 
     * thread execution starts
     */
    public abstract void doRun();

    /**
     * Implemented from runnable
     */
    @Override
    public final void run() {
       
            doRun();
            notifyListeners();
    }
    
}