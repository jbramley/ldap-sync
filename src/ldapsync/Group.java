
package ldapsync;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * A class to represent a posix group
 * @author sf659
 */
public class Group {
    
    
    private String commonName;
    private String gidNumber;
    private String dn;
    private Set<String> members;
    private boolean hasMembers = false;
    private GroupType type = null;
    
    /**
     * Creates a group object without any attributes
     */
    public Group(){
        
    }
    
    public enum GroupType{
        
        USER,
        
        ACCOUNT,
    }
    
    /**
     * Creates a group object with the following attributes
     * @param gid GID number of the group
     * @param cn Common name of the group
     */
    public Group(String gid, String cn){
        gidNumber = gid;
        commonName = cn;
    }
    
    /**
     * Sets this groups type, groups fall into two categories,
     * user and account, a user group is a group soley for one user, so
     * that each user is at least a member of their own group. An account group
     * defines a staff, postgrad extra group.
     * @param type GroupType for this group
     */
    public void setGrouptype(GroupType type){
        this.type = type;
    }
    
    /**
     * Returns the current grouptype
     * @return type current GroupType
     */
    public GroupType getGrouptype(){
        return type;
    }
    
    /**
     * Sets the common name
     * @param cn common name to set
     */
    public void setGroupName(String cn){
        commonName = cn;
        
    }
    
    /**
     * Sets the gid number
     * @param gid gidnumber to set
     */
    public void setGidNumber(String gid){
        gidNumber = gid;
    }
    
    /**
     * Returns groups name
     * @return commonname field
     */
    public String getGroupName(){
        return commonName;
    }
    
    /**
     * Returns groups gid number
     * @return gidNumber field
     */
    public String getGidNumber(){
        return gidNumber;
    }
    
    /**
     * Sets groups Distinguished Name
     * @param dn distinguished name to set
     */
    public void setGroupDN(String dn){
        this.dn = dn;
    }
    
    /**
     * Returns groups distinguished name
     * @return dn field
     */
    public String getDN(){
        return dn;
    }
    
    /**
     * Sets a list of members for this group
     * @param members array of members to add
     */
    public void setMembers(String[] members){
       this.members = new HashSet<String>(Arrays.asList(members));
      
    }
    
    /**
     * Adds a user to this group
     * @param user user to add
     */
    public void addMember(User user){
        members.add(user.getUsername());
    }
    
    /**
     * Removes a user from this group
     * @param user user to remove
     */
    public void removeUser(User user){
        members.remove(user.getUsername());
    }
    
     /**
     * Checks if a user is a member of this group
     * @param user user to check
     */
    public boolean hasMember(User user){
        if(members.contains(user.getUsername())){
            return true;
        }
        return false;
    }
    
    /**
     * Returns the set of all members of this group
     * @return hashset of all members
     */
    public HashSet getMembers(){
        return (HashSet)members;
    }
    
    /**
     * Retrieves groups hashCode representation based on gidNumber
     * @return hashcode
     */
    @Override
    public int hashCode(){
        return getGidNumber().hashCode();
    }

    /**
     * Checks if two groups are equal
     * @param obj group to check against
     * @return true if groups are equal
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Group other = (Group) obj;
        return true;
    }
    
}
