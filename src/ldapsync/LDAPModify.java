
package ldapsync;


import com.unboundid.ldap.sdk.Attribute;
import com.unboundid.ldap.sdk.DeleteRequest;
import com.unboundid.ldap.sdk.Entry;
import com.unboundid.ldap.sdk.LDAPConnection;
import com.unboundid.ldap.sdk.LDAPException;
import com.unboundid.ldap.sdk.LDAPResult;
import com.unboundid.ldap.sdk.Modification;
import com.unboundid.ldap.sdk.ModifyDNRequest;
import com.unboundid.ldap.sdk.ResultCode;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * The LDAPModify class is an extension of the {@link LDAPOperator} class. It is invoked
 * when modifications to the ldap entries are required<p>
 * There are 4 possible modification operations that can be preformed.<p>
 * 1) ADD - used for adding new ldap entries. <p>
 * 2) DELETE - used for deleting ldap entries. <p>
 * 3) MODIFY - used for modifying attributes of ldap entries.<p>
 * 4) MODIFYDN - used for changing the  distinguished name of ldap entries. <p>
 * 
 * Each LDAPModify is passed a hashmap of operations and an modification type, these
 * operations will then be preformed depending on the modification type.
 * @author sf659
 */
public class LDAPModify extends LDAPOperator {

    private String hostname;
    private String bindpasswd;
    private String binduser;
    private int port;
    private LDAPConnection connection;
    private Operation type;
    private ResultCode result;
    /** Operations to preform*/
    private HashMap<String, Attribute[]> operations;

    /**
     * Enum to describe available operations
     */
    public enum Operation {

        /**
         * Defines a add operation
         */
        ADD,
        /**
         * Defines a delete operation
         */
        DELETE,
        /**
         * Defines a modify operation
         */
        MODIFY,
        /**
         * Defines a modifydn operation
         */
        MODIFYDN
    }

    /**
     * Creates a new LDAPModify based on the following parameters
     * @param hostname  hostname of ldap server
     * @param port  port of ldap server
     * @param binduser  user to preform modification
     * @param bindpasswd  password for user
     * @param type type of operation to preform {@link Operation}
     * @param operations a hashmap of operations to preform
     */
    public LDAPModify(String hostname, int port, String binduser, String bindpasswd, Operation type, HashMap operations) {

        this.hostname = hostname;
        this.binduser = binduser;
        this.bindpasswd = bindpasswd;
        this.port = port;
        this.operations = operations;
        this.type = type;
       
    }

    /**
     * Based on the current operation type {@link Operation} we will preform the required operations.
     * All operations are passed in a hashmap {@link operations}, the key is the DN we will be preforming the 
     * operation on and the value will be the operation or an array of operations that will be preformed.
     * @throws LDAPException 
     */
    private void preformOperations() throws LDAPException {

        Iterator modifications = operations.entrySet().iterator();

        Logger.getLogger(LDAPModify.class.getName()).log(Level.INFO, "Preforming {0} operations", type);
        switch (type) {
            case MODIFY:
                //We are passed an array of modification objects as the value of the hashmap, 
                 Logger.getLogger(LDAPModify.class.getName()).log(Level.INFO, 
                            "{0} MODIFICATION Operations to preform ", new Object[]{operations.size()});
             
                 while (modifications.hasNext()) {
                    Map.Entry modPair = (Map.Entry) modifications.next();
                    String dn = (String) modPair.getKey();
                    List<Modification> mod = (List<Modification>) modPair.getValue();
                    LDAPResult ldapResult  = connection.modify(dn, mod);
                    
                    Logger.getLogger(LDAPModify.class.getName()).log(Level.INFO,
                            "MODIFY Resquest for {0} {1}\n", new Object[]{dn, ldapResult});
                        
                   //Dont log  the password hash
                   for (Modification modi : mod ){
                       if ( !"userPassword".equals(modi.getAttributeName())){
                          Logger.getLogger(LDAPModify.class.getName()).log(Level.INFO, "MODIFY {0} \n", new Object[]{modi});
                       }else{
                           Logger.getLogger(LDAPModify.class.getName()).log(Level.INFO, "Updating password\n");
                       }  
                   }  
                }
             break;

            case ADD:
                Logger.getLogger(LDAPModify.class.getName()).log(Level.INFO,
                        "{0} ADD Operations to preform", new Object[]{operations.size()});
             
                while(modifications.hasNext()){
                    Map.Entry modPair = (Map.Entry) modifications.next();
                    String dn = (String)modPair.getKey();
                    
                    Entry entry = (Entry)modPair.getValue();
                    LDAPResult ldapResult  = connection.add(entry);
                    Logger.getLogger(LDAPModify.class.getName()).log(Level.INFO, 
                            "ADD Resquest for {0} {1}\n", new Object[]{entry.getDN(), ldapResult});                  
                }
                 break;
            
            case DELETE:
                 Logger.getLogger(LDAPModify.class.getName()).log(Level.INFO, 
                         "{0} DELETE Operations to preform", new Object[]{operations.size()});
                 while(modifications.hasNext()){
                   Map.Entry modPair = (Map.Entry) modifications.next();
                   String dn = (String) modPair.getKey();
                   DeleteRequest deleteReq = (DeleteRequest)modPair.getValue();
                   LDAPResult ldapResult = connection.delete(deleteReq);
                   
                   Logger.getLogger(LDAPModify.class.getName()).log(Level.INFO, 
                           "DELETE request forfor {0} {1}\n", new Object[]{deleteReq.getDN(), ldapResult});

                 }
                break;

            case MODIFYDN:
                  Logger.getLogger(LDAPModify.class.getName()).log(Level.INFO, 
                          "{0} MODIFYDN Operations to preform", new Object[]{operations.size()});
                
                 while(modifications.hasNext()){
                     Map.Entry modPair = (Map.Entry)modifications.next();
                     String dn = (String)modPair.getKey();
                     ModifyDNRequest modDN = (ModifyDNRequest)modPair.getValue();
                     LDAPResult ldapResult  = connection.modifyDN(modDN);
                     
                     Logger.getLogger(LDAPModify.class.getName()).log(Level.INFO, 
                             "MODIFY Request for {0} {1}\n", new Object[]{dn, ldapResult});
 
                 }  
                break;
        }
        //Remember to clear the operations hash map afterwards, other wise we will
        //end up processing modifications / additions multiply times
        operations.clear();
    }

    /**
     * The NotificationThread run method, when this thread is scheduled to run this method is executed
     */
    @Override
    public void doRun() {
        try {
            try {
                connection = ldapConnect(hostname, port);

            } catch (LDAPException ex) {
                exceptionOccured(ex, "Could not connect to ldap server");
            } catch (NoSuchAlgorithmException ex) {
                exceptionOccured(ex, "No Such algorithm");
            } catch (KeyStoreException ex) {
                exceptionOccured(ex, "There is a problem with the internal keystore");
            } catch (IOException ex) {
                exceptionOccured(ex);
            } catch (CertificateException ex) {
                exceptionOccured(ex, "Certification failure");
            } catch (GeneralSecurityException ex) {
                exceptionOccured(ex);
            }
            
            connection = ldapBind(binduser, bindpasswd);
         
        } catch (LDAPException ex) {
            exceptionOccured(ex);
        } 
        try {
            
            preformOperations();
            
        } catch (LDAPException ex) {
           //We will treat not been able to complete one of the operations as non fatal,
            // and let the scheduler decide what to do 
            nonFatalExceptionOccured(ex);
        }finally{
            //Clear the operations out also incase of an exception is raised
            operations.clear();
            connection.close();
        }
    }
    
   /**
     * Ensures any open connections are closed before an exit
     */
    @Override
    public void setshutdownHook() {
        //Shutdown Hook
            Runtime.getRuntime().addShutdownHook(new Thread(){
            @Override
                public void run(){
                if(connection != null){
                    //The connection may have already been closed
                    connection.close();
                       }
                Logger.getLogger(LDAPModify.class.getName()).log(Level.FINE, "Shutting down LDAPModify thread \n");
                }
            });

    }
    
}
