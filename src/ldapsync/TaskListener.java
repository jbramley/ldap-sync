/*
 * 
 * 
 */
package ldapsync;


/**
 * An interface for a TaskListener, a TaskListener is added to a {@link NotificationThread}
 * so that it can be informed when exceptions or certain actions within the thread have
 * finished
 * @author sf659
 */
public interface TaskListener {
    
    /**
     * Invoked when a NotificationThread has completed its doRun() method.
     * @param runner  The current NotificationThread that has finished execution
     */
    public void threadComplete( NotificationThread runner );
    
    /**
     * Invoked when an exception occurs in NotificicationThread runner.
     * @param runner  The NotificationThread where the exception occured
     * @param ex  The exception caught by the NotificationThread runner
     */
    public void exceptionOccured(NotificationThread runner, Exception ex);
    
    /**
     * Invoked when an exception occurs in NotificicationThread runner.
     * @param runner  The NotificationThread where the exception occured
     * @param ex The exception caught by the NotificationThread runner
     * @param msg  A descriptive message about the exception
     */
    public void exceptionOccured(NotificationThread runner, Exception ex, String msg);
    
    /**
     * Invoked when an exception occurs in  NotificicationThread runner, this differs from
     * a exceptionOccured invocation in that the NotificationThread runner has signaled 
     * this may not be a fatal exception, the TaskListener will have to decide.
     * @param runner  The NotificationThread where the exception occured
     * @param ex  The exception caught by the NotificationThread runner
     */
    public void nonFatalExceptionOccured(NotificationThread runner, Exception ex);
    
    /**
     * Invoked when a group of tasks needs to be scheduled
     * @param notificationGroup  A new NotificationThreadGroup that contains the NotificationThreads
     * that need to be scheduled.
     */
    public void scheduleEvents(NotificationThreadGroup notificationGroup);
    
}