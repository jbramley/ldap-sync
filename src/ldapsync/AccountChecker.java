package ldapsync;

import com.unboundid.ldap.sdk.DeleteRequest;
import com.unboundid.ldap.sdk.Entry;
import com.unboundid.ldap.sdk.Modification;
import com.unboundid.ldap.sdk.ModificationType;
import com.unboundid.ldap.sdk.ModifyDNRequest;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * The AccountChecker class preforms a range of checks on the accounts using information extracted from 
 * the {@link CentralPasswordRetriever}, {@link DatawarehouseReader} and the {@link LDAPSearch}</p>
 * 
 * These checks include, that the current university held password and the one in the ldap server are equal,
 * that a students resides within the correct organisational unit, that the user has a valid posixgroup and finally
 * that the user account is valid.</p>
 * 
 * If an account cannot be found in the ldap server a new account for students is generated based on the users details extracted from
 * both the {@link CentralPasswordRetriever} and the {@link DatawarehouseReader}. In the case of departmental and staff accounts this information
 * is extracted soley from the {@link CentralPasswordRetriever}.</p>
 * 
 * If an account is deemed to no longer exist within the universities system then the account will be disabled and can be manually deleted.
 * @author sf659
 */
public class AccountChecker extends NotificationThread {

    private HashMap studentsDatawarehouse;
    private HashMap currentLdapUsers;
    private HashMap currentLdapGroups;
    /** {@link java.util.HashMap} for modification operations LDIF: MODIFY */
    private HashMap<String, List<Modification>> modifyOperations;
    /** {@link java.util.HashMap} for add operations LDIF: ADD */
    private HashMap<String, Entry> addOperations;
    /** {@link java.util.HashMap}for modification to a DN operations LDIF: MODDN */
    private HashMap<String, ModifyDNRequest> modDNOperations;
    /** {@link java.util.HashMap} for delete operations LDIF: DELETE */
    private HashMap<String, DeleteRequest> deleteOperations;
    private File currentPasswordFile;
    /** Column where the UID (username) resides in /etc/passwd */
    private static final int UID = 0;
    /** Column where the users password resides in /etc/passwd */
    private static final int PASSWORD = 1;
    /** Column where the users UID Number resides in /etc/passwd */
    private static final int UIDNUMBER = 2;
    /** Column where the users GID Number resides in /etc/passwd */
    private static final int GIDNUMBER = 3;
    /** Column where the users gecos resides in /etc/passwd */
    private static final int GECOS = 4;
    private static final String BASE = "dc=physics,dc=local";
    private static final String BASE_USER_DN = "ou=users,dc=physics,dc=local";
    private static final String BASE_GROUP_DN = "ou=usergroups,ou=groups,dc=physics,dc=local";
    private static final String BASE_STUDENT_GROUP_DN = "ou=students," + BASE_USER_DN;
    private boolean test;
    private int smallTest = 0;
    /** HashSet contains all valid university usernames */
    private HashSet<String> uniUsers;
    private SecureRandom random;

    /**
     * Creates a new AccountChecker using the following required retrived details
     * @param studentDatawarehouse A hashmap which should be populated with {@link Student} objects from the {@link DatawarehouseReader}
     * @param currentLdapUsers  A hashmap which should be populated with {@link User} object from the {@link LDAPSearch}
     * @param currentLdapGroups 
     * @param currentPasswordfile A password file retrived from the {@link CentralPasswordRetriever}
     */
    public AccountChecker(HashMap studentDatawarehouse, HashMap currentLdapUsers, HashMap currentLdapGroups, File currentPasswordfile) {

        this.studentsDatawarehouse = studentDatawarehouse;
        this.currentLdapUsers = currentLdapUsers;
        this.currentPasswordFile = currentPasswordfile;
        this.currentLdapGroups = currentLdapGroups;

        //There are 4 different types of operations that we are going to preform on accounts, these
        //operations are placed into 4 seperate hashmaps depending on their requirements
        // 1) General modifications to attributes such as passwords, these go in modifyOperations
        // 2) Add operations for creating new users, these go in addOperations
        // 3) Moving a user to a new organisational unit, these go in modDNOperations.
        // 4) Delete opertation involve removing an object

        modifyOperations = new HashMap<String, List<Modification>>();
        addOperations = new HashMap<String, Entry>();
        modDNOperations = new HashMap<String, ModifyDNRequest>();
        deleteOperations = new HashMap<String, DeleteRequest>();

        uniUsers = new HashSet<String>();

        test = true;


    }

    /**
     * Preforms the start of the account checking, we must first loop over the password file
     * and find the accounts we are intressted in, these fall into four categories.</p>
     * 
     * 1) If GID == 1005 and the the username is found within the populated studentsDatawarehouse hashmap,
     * this means the user is a postgraduate.</p>
     * 
     * 2) If GID == 1005 but the username is not found within the populated studentsDatawarehouse hashmap, 
     * this means the user is a member of staff.</p>
     * 
     * 3) If the username is just found in the populated studentsDatawarehouse hashmap, 
     * this means the user is an undergraduate.</p>
     * 
     * 4) If the current user is found in ldap but their GID != 1005, this means one of two things
     * either its an old physics account or an associate account. </p>
     * 
     * Once we have determined the exact nature of the user we can then pass this user to the processAccount() method</p>
     */
    private void accountCheck() throws FileNotFoundException, IOException {
        BufferedReader br = new BufferedReader(new FileReader(currentPasswordFile));
        System.out.println("Account check running ################################################################");
        for (String line; (line = br.readLine()) != null;) {

            String[] accountParts = line.split((":"));
            // Store a copy in uniUsers
            uniUsers.add(accountParts[UID]);

            if ("1005".equals(accountParts[GIDNUMBER]) && studentsDatawarehouse.containsKey(accountParts[UID])) {
                // This equals a post grad student
                // Retrive the current student object from the data warehouse results
                Student student = (Student) studentsDatawarehouse.get(accountParts[UID]);
                student.setPassword(accountParts[PASSWORD]);
                student.setuidNumber(accountParts[UIDNUMBER]);
                student.setFullname(accountParts[GECOS]);
                student.setAccountType(User.Type.STUDENT);
                processAccount(student);
                

            } else if ("1005".equals(accountParts[GIDNUMBER]) && !studentsDatawarehouse.containsKey(accountParts[UID])) {
                // this is staff or department account

                User user = new User();
                user.setUsername(accountParts[UID]);
                user.setPassword(accountParts[PASSWORD]);
                user.setuidNumber(accountParts[UIDNUMBER]);
                user.setFullname(accountParts[GECOS]);

                if (isDeparmentalAccount(user)) {
                    user.setAccountType(User.Type.DEPARTMENT);
                    processAccount(user);
                } else {
                    user.setAccountType(User.Type.STAFF);
                    processAccount(user);
                }

            } else if (studentsDatawarehouse.containsKey(accountParts[UID])) {
                // this is a student account


                Student student = (Student) studentsDatawarehouse.get(accountParts[UID]);
                student.setAccountType(User.Type.STUDENT);
                student.setPassword(accountParts[PASSWORD]);
                student.setuidNumber(accountParts[UIDNUMBER]);
                student.setFullname(accountParts[GECOS]);
                processAccount(student);
                
            } else if (currentLdapUsers.containsKey(accountParts[UID]) && !"1005".equals(accountParts[GIDNUMBER])){
                // Our final check will find associate accounts and those of users
                // who were originally physics but not have most likely moved from the department
                // in which case we shall disable the latter.
                User ldapUser = (User)currentLdapUsers.get(accountParts[UID]);
              
                if(ldapUser.getAccountTypeEnum() == User.Type.ASSOCIATE){
                    User user = new User();
                    user.setUsername(accountParts[UID]);
                    user.setPassword(accountParts[PASSWORD]);
                    user.setuidNumber(accountParts[UIDNUMBER]);
                    user.setFullname(accountParts[GECOS]);
                    user.setAccountType(User.Type.ASSOCIATE);
                    processAccount(user);

                }else{
                    disableAccount(ldapUser);
                    
                }
                
            }

        }
        // Once all the accounts are processed we can check that our current
        // ldap users are still valid university users
        checkforRemovedAccounts();
    }

   

    /**
     * Interate over all our found ldap users (these are users currently
     * in our ldap server and found in {@link ldapsysnc.AccountChecker.currentLdapUsers})  and check to see
     * if they are currently in the hashset {@link ldapsysnc.AccountChecker.uniUsers} which contains all valid 
     * university user accounts, if the account if not found we disable the account
     */
    private void checkforRemovedAccounts() {
        Iterator ldapUserIterator = currentLdapUsers.keySet().iterator();

        while (ldapUserIterator.hasNext()) {
            String ldapUser = (String) ldapUserIterator.next();
            if (!uniUsers.contains(ldapUser)) {
                User user = (User) currentLdapUsers.get(ldapUser);
                disableAccount(user);

            }

        }

    }

    /**
     * Checks to see if the user account is a departmental account
     * @param user User account to check
     * @return True if the users username matches the following regex <b> phys[0-9]+ </b>
     * 
     */
    private boolean isDeparmentalAccount(User user) {
        String regexPhysicsDepartmentAccounts = "phys[0-9]+";
        return user.getUsername().matches(regexPhysicsDepartmentAccounts);

    }

    /**
     * Checks the current user account, either creating a new account or updating
     * the details of an exisiting account
     * @param user User account we are checking
     */
    private void processAccount(User user) {
        // The uniUser object will be a user object created from the details from unix0
        // (ypcat passwd).
        // userFoundinLdap will be a user object created from details obtained from the ldapserver
        // An equality test between uniUser.equals(userFoundinLdap) will equal true, (equality is based on username)
        // however if the passwords are not equal we know that the current ldap password is incorrect.

        if (!currentLdapUsers.containsKey(user.getUsername())) {
            // This means the user is not currently present in the populated hashmap of current ldap users,
            // meaning they currently do not have an account in the ldap server so we should create one
            createNewUser(user);

        } else {
            // Here we will extract the corresponding ldap entry for this user
            // We know here the user definitely has an account in the ldap server but we would like to check that their 
            // current details are correct.
            User userFoundinLdap = (User) currentLdapUsers.get(user.getUsername());

            if (!passwordsEqual(user.getPassword(), userFoundinLdap.getPassword())) {
                //Always update ldap password with uni password if they do not match
                updatePassword(userFoundinLdap, user.getPassword());
            }

            // Check if the user has a valid group if not create one, this is just a safety check,
            // incase the group was deleted externally 
            if (!currentLdapGroups.containsKey(user.getUsername())) {
                createUserGroup(userFoundinLdap);
            }

            // This is a special edge case where a postgrad has normaly finished their
            // program but still has a valid unix account, at the moment we shall 
            // not process the account any more appart from making any password changes.
            // In the future we may wish to move the account to a holding OU. The account is 
            // actually categorised by the accountCheck() method as a staff account, As the following 
            // conditions hold true;
            // gid == 1005 and they are not in the student data warehouse.
            // as we pass this user object from the accountCheck method to checkAccGroupMembership
            // without this check this leads to a situation where it tries to add the user to the staff group if
            // we do not catch the case.
 
            if (userFoundinLdap.getAccountTypeEnum() == User.Type.STUDENT && 
                        !studentsDatawarehouse.containsKey(userFoundinLdap.getUsername())){
                Logger.getLogger(AccountChecker.class.getName()).log(Level.INFO, "Ignoring postgrad account {0}, we suspect this user has graduated ",userFoundinLdap.getUsername() );
                
            }else {
                //need to check if user is in correct OU
                correctOU(userFoundinLdap);
                checkAccGroupMembership(userFoundinLdap);
            }
        }
    }

   
    /**
     * As students move from year to year the organisational group they are a member
     * of will change, here we check that the current group is valid and if not
     * prepare a modification.
     * @param user LDAP Account we are checking
     */
    private void correctOU(User user) {

        switch (user.getAccountTypeEnum()) {
            case STUDENT:
                // User is the object extract from LDAP
                // We extract the student object from the data warehouse which has the same
                // username, this will allow us to access the current students details

                Student student = (Student) studentsDatawarehouse.get(user.getUsername());
                student.setDN(user.getDN());
                
                // This is students actual year as gathered from the data warehouse
                Student.Year currentYear = student.getYear();

                // This is the year based on the students current dn, if current != ldapReportedYear,
                // the student must be moved to a new OU
                Student.Year ldapReportedYear = student.getYear(student.getDN());

                if (currentYear != ldapReportedYear) {
                    // need to update DN
                    String newSuperior = student.getOU(currentYear);
                    newSuperior = newSuperior + "," + BASE_STUDENT_GROUP_DN;

                    String rdn = "uid=" + student.getUsername();
                    ModifyDNRequest modDN = new ModifyDNRequest(student.getDN(), rdn, true, newSuperior);
                    modDNOperations.put(student.getDN(), modDN);

                    //We need also to move the users account type 
                    String accGroupStr = student.getAccountGroup();
                    Group accGroup = (Group) currentLdapGroups.get(accGroupStr);
 
                    String newAccGroupStr = student.getAccountGroup(currentYear);
                    Group newAccGRoup = (Group) currentLdapGroups.get(newAccGroupStr);

                    if (accGroup.hasMember(user) && !accGroupStr.equals(newAccGroupStr)) {
                        List<Modification> currentMods;
                        //Remove old membership
                        Modification modifyAccGroupMembership = new Modification(ModificationType.DELETE, "memberUid", user.getUsername());
                        addToModificationsOperations(accGroup.getDN(), modifyAccGroupMembership);
 
                        //Add new membership
                        Modification addAccGroupModification = new Modification(ModificationType.ADD, "memberUid", user.getUsername());
                        addToModificationsOperations(newAccGRoup.getDN(), addAccGroupModification);

                    }

                }
                //end of student case
                break;
        }

    }
    
     /**
     * Creates a valid user group
     * @param user The user account a group needs creating for.
     */
    private void createUserGroup(User user) {
        String groupBase = "";
        String groupDN = "cn=" + user.getUsername();

        switch (user.getAccountTypeEnum()) {

            case STUDENT:
                groupBase = "ou=studentgroups," + BASE_GROUP_DN;
                break;
            case STAFF:
                groupBase = "ou=staffgroups," + BASE_GROUP_DN;
                break;
            case ASSOCIATE:
                groupBase = "ou=associategroups," + BASE_GROUP_DN;
                break;
        }

        groupDN = groupDN + "," + groupBase;

        Entry newGroup = new Entry(groupDN);
        newGroup.addAttribute("objectClass", "namedObject");
        newGroup.addAttribute("objectClass", "posixGroup");
        newGroup.addAttribute("gidNumber", user.getUidNumber());
        newGroup.addAttribute("cn", user.getUsername());
        newGroup.addAttribute("memberUid", user.getUsername());

        addOperations.put(groupDN, newGroup);
    }

     /**
     * Add modification operations, we may have multiple operations
     * per Distinguished Name so need to add our new operation to the end
     * of the list.
     * @param dn distinguished name for operation
     * @param mod modification to preform
     */
    private void addToModificationsOperations(String dn, Modification mod){
        List currentMods;
        if(modifyOperations.containsKey(dn)){
            currentMods = modifyOperations.get(dn);
            currentMods.add(mod);
            modifyOperations.put(dn, currentMods);
        }else{
            currentMods = new ArrayList<Modification>();
            currentMods.add(mod);
            modifyOperations.put(dn, currentMods);
 
        }
    }

     /**
     * Checks if an account group contains a user if not add the user.
     * @param user user to check group membership
     */
    private void checkAccGroupMembership(User user) {

        String accGroupStr = user.getAccountGroup();
        
        // Check the account group actually exists, it should do
        if (currentLdapGroups.containsKey(accGroupStr)) {
            Group accGroup = (Group) currentLdapGroups.get(accGroupStr);

            if (!accGroup.hasMember(user)) {
                //Need to add member to group
                List<Modification> currentMods;
                Modification modifyMembership = new Modification(ModificationType.ADD, "memberUID", user.getUsername());
                addToModificationsOperations(accGroup.getDN(), modifyMembership);
            }
        }
    }

    /**
     * Checks if the two passwords are equal
     * @param unipassword The password found from the file retrieved from {@link CentralPasswordRetriever}
     * @param ldapPassword The password found for the user in the ldapusers hashmap populated from {@link LDAPSearch}
     * @return boolean true or false dependind on if the passwords equal or not
     */
    private boolean passwordsEqual(String unipassword, String ldapPassword) {
        //All passwords are $1$ UNIX BSD Crypt, in the ldap server these are preceeded by {CRYPT}
        // so add this to the unipassword before checking

        String cryptUni = "{CRYPT}" + unipassword;
        if (ldapPassword.equals(cryptUni)) {
            return true;
        }
        return false;
    }

    /**
     * Adds the password update request to the modificationOperation hashmap 
     * @param user User who's password needs updating
     * @param newPassword New password for user 
     */
    private void updatePassword(User user, String newPassword) {

        String cryptNewPassword = "{CRYPT}" + newPassword;
        Modification modifyOperation = new Modification(ModificationType.REPLACE, "userPassword", cryptNewPassword);
        modifyOperations.put(user.getDN(), Arrays.asList(modifyOperation));

    }

    /**
     * Generates a new user request to the addOperation hashmap, these are then processed later
     * and create the new accounts
     * @param user The user for who we wish to create an account
     */
    private void createNewUser(User user) {

        //As this is a new user the dn will not yet have been set, and there will be
        // no user group for the user
        String accountClass = "";

        String userGroup = "";

        String accGroupStr = "";

        //We use this switch to determine which class the user is, this will effect
        // the final Distinguished Name which will include which organisational unit 
        // the user will end up in
        switch (user.getAccountTypeEnum()) {
            case STUDENT:
                Student student = new Student();
                if (user instanceof Student) {
                    student = (Student) user;
                    switch (student.getYear()) {
                        case FOUNDATION:
                            accountClass = "ou=foundation,ou=students";
                            accGroupStr = "foundation";
                            break;
                        case FIRST:
                            accountClass = "ou=firstyear,ou=students";
                            accGroupStr = "firstyear";
                            break;
                        case SECOND:
                            accountClass = "ou=secondyear,ou=students";
                            accGroupStr = "secondyear";
                            break;
                        case THIRD:
                            accountClass = "ou=thirdyear,ou=students";
                            accGroupStr = "thirdyear";
                            break;
                        case FOURTH:
                            accountClass = "ou=fourthyear,ou=students";
                            accGroupStr = "fourthyear";
                            break;
                        case POSTGRAD:
                            accountClass = "ou=postgrad,ou=students";
                            accGroupStr = "postgrad";
                            break;
                        case UNKNOWN:
                            Logger.getLogger(CentralPasswordRetriever.class.getName()).log(Level.SEVERE, "Failed to find year for student {0}", student.getUsername());
                            break;
                    }

                }
                userGroup = "ou=studentgroups";
                break;
            case STAFF:
                accountClass = "ou=staff";
                userGroup = "ou=staffgroups";
                accGroupStr = "staff";
                break;
            case DEPARTMENT:
                accountClass = "ou=departmental";
                userGroup = "ou=departmentalgroups";
                accGroupStr = "departmental";
                break;

        }

        String userDN = "uid=" + user.getUsername() + "," + accountClass + "," + BASE_USER_DN;
        String groupDN = "cn=" + user.getUsername() + "," + userGroup + "," + BASE_GROUP_DN;


        String username = user.getUsername();

        //Create a new entry based on the users details
        Entry newUser = new Entry(userDN);
        newUser.addAttribute("objectClass", "account");
        newUser.addAttribute("objectClass", "accountExtras");
        newUser.addAttribute("objectClass", "posixAccount");
        newUser.addAttribute("objectClass", "shadowAccount");
        newUser.addAttribute("objectClass", "top");
        newUser.addAttribute("cn", username);

        //set gid to uid
        newUser.addAttribute("gidNumber", user.getUidNumber());
        newUser.setAttribute("homeDirectory", "/home/" + username);
        newUser.setAttribute("uidNumber", user.getUidNumber());
        newUser.setAttribute("accountType", user.getAccountType());
        newUser.setAttribute("gecos", user.getFullName());
        newUser.setAttribute("loginShell", "/bin/bash");
        newUser.setAttribute("shadowLastChange", "14981");
        newUser.setAttribute("shadowMax", "9999");
        newUser.setAttribute("shadowWarning", "7");
        newUser.setAttribute("userPassword", "{CRYPT}" + user.getPassword());


        addOperations.put(userDN, newUser);

        if (!currentLdapGroups.containsKey(user.getUsername())) {
            Entry newGroup = new Entry(groupDN);

            newGroup.addAttribute("objectClass", "top");
            newGroup.addAttribute("objectClass", "posixGroup");
            newGroup.addAttribute("objectClass", "namedObject");
            newGroup.addAttribute("cn", user.getUsername());
            newGroup.addAttribute("gidNumber", user.getUidNumber());
            newGroup.addAttribute("memberUid", user.getUsername());

            addOperations.put(groupDN, newGroup);
        }

        Group accGroup = (Group)currentLdapGroups.get(accGroupStr);
        
        if(!accGroup.hasMember(user)){
        //Add the user to the appropriate group
            Modification modifyMembership = new Modification(ModificationType.ADD, "memberUID", user.getUsername());
            addToModificationsOperations(accGroup.getDN(), modifyMembership);
        }
    }

    /**
     * As we dont want to ever delete an user account incase something went a miss,
     * we will simple disable the user account, this is done by moving the user 
     * to a new disabledaccounts organisational unit, setting their loginshell to /bin/false
     * and updating the password with a random string
     * @param user Account we wish to disable
     */
    private void disableAccount(User user) {
        random = new SecureRandom();
        String randomPassword = nextRandomString();

        Modification modifyPassword = new Modification(ModificationType.REPLACE, "userPassword", randomPassword);
        Modification modifyLogin = new Modification(ModificationType.REPLACE, "loginShell", "/bin/false");
        Modification modifyAccountType = new Modification(ModificationType.REPLACE, "accountType", "disabled");
        Modification modifyGecos = new Modification(ModificationType.REPLACE, "gecos", "disabled");

        String accGroupString = user.getAccountGroup();
        Group accGroup = (Group) currentLdapGroups.get(accGroupString);

        String rdn = "uid=" + user.getUsername();
        String newSuperior = "ou=disabledaccounts," + BASE;
        ModifyDNRequest modDN = new ModifyDNRequest(user.getDN(), rdn, true, newSuperior);

        Group userGroup = (Group) currentLdapGroups.get(user.getUsername());
        DeleteRequest del = new DeleteRequest(userGroup.getDN());

        if (accGroup.hasMember(user)) {
            Modification modifyAccGroupMembership = new Modification(ModificationType.DELETE, "memberUid", user.getUsername());
            addToModificationsOperations(accGroup.getDN(), modifyAccGroupMembership);
           
        }

        modifyOperations.put(user.getDN(), Arrays.asList(modifyPassword, modifyLogin, modifyAccountType, modifyGecos));
        modDNOperations.put(user.getDN(), modDN);

        //We only ever delete the users group never the full user object, this is
        // just incase there is a mistake, a user object within the users organistation unit
        // will automatically have there group recreated if has been deleted.
        deleteOperations.put(userGroup.getDN(), del);

    }

    /**
     * Generates a random string we can use for a password
     * @return A newly generated random string
     */
    private String nextRandomString() {
        return new BigInteger(130, random).toString(32);
    }

    
     /**
     * The NotificationThread run method, when this thread is scheduled to run this method is executed
     * 
     */
    @Override
    public void doRun() {
        try {
            accountCheck();

            ArrayList mods = new ArrayList(4);

            //We will only start a LDAPModify worker thread if there is some data to process,
            // this happens when the corrosponding operations hashmap has an entry
            if (modifyOperations.size() > 0) {
                LDAPModify modify = new LDAPModify(Config.ldapserver, Config.ldapport, Config.binduser,
                        Config.binduserpasswd, LDAPModify.Operation.MODIFY, modifyOperations);
                mods.add(modify);
            }

            if (addOperations.size() > 0) {

                LDAPModify add = new LDAPModify(Config.ldapserver, Config.ldapport, Config.binduser,
                        Config.binduserpasswd, LDAPModify.Operation.ADD, addOperations);
                mods.add(add);
            }


            if (deleteOperations.size() > 0) {
                LDAPModify delete = new LDAPModify(Config.ldapserver, Config.ldapport, Config.binduser,
                        Config.binduserpasswd, LDAPModify.Operation.DELETE, deleteOperations);
                mods.add(delete);
            }

            List modOps = Arrays.asList(mods.toArray());

            LDAPModify modifyDN = new LDAPModify(Config.ldapserver, Config.ldapport, Config.binduser,
                    Config.binduserpasswd, LDAPModify.Operation.MODIFYDN, modDNOperations);

            NotificationThreadGroup modifications = null;


            // Logic Explained:
            // modOps will contain all the operations if there are any, in the first case if
            // there is no current delete add or modify operations then check if modDNOperations
            // contains any modifications to execute, if it does, add it do the modOps list and 
            // schedule the job. 
            // Next if modOperations is empty just schecule the exisitng jobs, if its not empty schedule 
            // the existing jobs followed by the modDN operations

            if (modOps.isEmpty()) {
                if (!modDNOperations.isEmpty()) {
                    mods.add(modifyDN);
                    //Rebuilt array, Arrays.asList: Returns a fixed-size list backed by the specified array.
                    //Cannot have items added or removed
                    modOps = Arrays.asList(mods.toArray());
                    modifications = new NotificationThreadGroup(modOps, true);
                }
            } else {
                if (modDNOperations.isEmpty()) {
                    modifications = new NotificationThreadGroup(modOps, true);
                } else {
                    modifications = new NotificationThreadGroup(modOps, modifyDN, true);
                }

            }

            // Finally if the list contains some operations to do 
            // schedule them
            if (modOps.size() > 0) {
                scheduleEvent(modifications);
            }


        } catch (FileNotFoundException ex) {
            exceptionOccured(ex, "Cannot find password file " + currentPasswordFile);
        } catch (IOException ex) {
            exceptionOccured(ex);
        } catch (NullPointerException ex) {
            exceptionOccured(ex);
        } catch (Exception ex) {
            System.out.println(ex);
        } finally {

            // We clear the hash maps at the end of the run, this ensures that if
            // an operation takes place outside of this application the data will be refreshed
            // on the next run. For example if a users group was deleted externally if we did not 
            // clear the hashmaps this group would still exist in currentLdapGroups on the next
            // scheduled run and thus the group would not be correctly re created as would be desired
            currentLdapGroups.clear();
            currentLdapUsers.clear();
            currentPasswordFile.delete();


        }
    }
    
    /**
     * Ensures any open connections are closed before an exit
     */
    @Override
    public void setshutdownHook() {

        Runtime.getRuntime().addShutdownHook(new Thread() {

            @Override
            public void run() {
                Logger.getLogger(CentralPasswordRetriever.class.getName()).log(Level.FINE, "AccountChecker shutdown\n");

            }
        });
    }
}
