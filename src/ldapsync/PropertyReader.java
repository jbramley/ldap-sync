
package ldapsync;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Simple class to read a a properties file
 * @author sf659
 */
public class PropertyReader {
    
    private Properties properties = new Properties();
    
    /**
     * Constructor for PropertyReader
     * @param filename Path of property file
     */
    public PropertyReader(String filename){
        try {
            
            properties.load(new FileInputStream(filename));       
            
        } catch (FileNotFoundException ex) {
            Logger.getLogger(PropertyReader.class.getName()).log(Level.SEVERE, "Properties file not found in local directory", ex);
           
            System.exit(-1);
        } catch (IOException ex) {
                Logger.getLogger(PropertyReader.class.getName()).log(Level.SEVERE, null, ex);
            }
         
    }
    
    /**
     * Retrieves properties from the property file
     * @param property The property name
     * @return The property value
     */
    public String returnProperty(String property){
        
        String prop = properties.getProperty(property);
        if (prop == null){
                throw new NullPointerException();
        }
        
        return prop;
        
    }
}
