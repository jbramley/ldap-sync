package ldapsync;

import com.unboundid.ldap.sdk.LDAPConnection;
import com.unboundid.ldap.sdk.LDAPException;
import com.unboundid.util.ssl.SSLUtil;
import java.io.IOException;
import java.io.InputStream;
import java.security.GeneralSecurityException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;

/**
 * All ldap operation classes extend this thread which provides the default connection
 * methods allowing the ldap operation classes ({@link LDAPModify}, {@link LDAPSearch}) to
 * connect to the ldap server.
 * @author sf659
 */
public abstract class LDAPOperator extends NotificationThread {

    private LDAPConnection connection;

    /**
     * Binds to the current ldap server
     * @param binduser user to bind
     * @param bindpasswd password for user
     * @return A valid binded connection on which to preform operations
     * @throws LDAPException
     */
    public LDAPConnection ldapBind(String binduser, String bindpasswd) throws LDAPException {

        //Shutdown Hook
        Runtime.getRuntime().addShutdownHook(new Thread() {

            @Override
            public void run() {
                if (connection != null) {
                    disconnect();
                }
                Logger.getLogger(LDAPConnection.class.getName()).log(Level.FINE, "LDAPConnection Shutdown \n");
            }
        });

        connection.bind(binduser, bindpasswd);
        return connection;
    }

    /**
     * Creates a new connection the the ldap server
     * @param hostname server to connect to
     * @param port port on which to connect
     * @return A valid connected connection which can then be binded
     * @throws LDAPException
     * @throws NoSuchAlgorithmException
     * @throws KeyStoreException
     * @throws IOException
     * @throws CertificateException
     * @throws GeneralSecurityException
     */
    public LDAPConnection ldapConnect(String hostname, int port) throws LDAPException, 
            NoSuchAlgorithmException, KeyStoreException, IOException, CertificateException, GeneralSecurityException {
        

        //Shutdown Hook
        Runtime.getRuntime().addShutdownHook(new Thread() {

            @Override
            public void run() {
                if (connection != null) {
                    disconnect();
                }
                Logger.getLogger(LDAPConnection.class.getName()).log(Level.FINE, "LDAPConnection Shutdown \n");

            }
        });
        
        connection = new LDAPConnection(setupSSL().createSSLSocketFactory());
        connection.connect(hostname, port);
        return connection;

    }

    /**
     * Disconnects from the ldap server
     */
    public void disconnect() {
        connection.close();
    }

    /**
     * Provides the setup required to connect to the ldap server via SSL
     * @return an SSLUtil object with the required certificates setup
     * @throws NoSuchAlgorithmException
     * @throws KeyStoreException
     * @throws IOException
     * @throws CertificateException 
     */
    private SSLUtil setupSSL() throws NoSuchAlgorithmException, KeyStoreException, IOException, CertificateException {
        TrustManagerFactory trustManagerFactory = TrustManagerFactory.getInstance("PKIX");
        KeyStore ks = KeyStore.getInstance(KeyStore.getDefaultType());
        char[] kspassword = "changeit".toCharArray();
        InputStream is = this.getClass().getResourceAsStream("certificates/certs");
        ks.load(is, kspassword);
        trustManagerFactory.init(ks);
        TrustManager trustManagers[] = trustManagerFactory.getTrustManagers();
        SSLUtil sslUtil = new SSLUtil(trustManagers);
        return sslUtil;

    }
}
