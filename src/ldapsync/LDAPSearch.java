package ldapsync;

import com.unboundid.ldap.sdk.LDAPConnection;
import com.unboundid.ldap.sdk.LDAPException;
import com.unboundid.ldap.sdk.LDAPSearchException;
import com.unboundid.ldap.sdk.SearchResult;
import com.unboundid.ldap.sdk.SearchResultEntry;
import com.unboundid.ldap.sdk.SearchScope;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * The LDAPSearch class is responsible for retrieving all the current users from the ldap server, it will 
 * use the unbounded api to do this</p>
 * 
 * https://www.unboundid.com/products/ldapsdk/</p>
 * 
 * The users details are retrieved, a new {@link User} object is created with these details and placed inside the searchResults hashmap
 * 
 * This class extends the  {@link LDAPOperator} class which provides the connection, bind and disconnection method
 * 
 * 
 * @author sf659
 */
public class LDAPSearch extends LDAPOperator {

    private String hostname;
    private String bindpasswd;
    private String binduser;
    private int port;
    private LDAPConnection connection;
    private HashMap searchResults;
    private SearchType searchType;
    private final String PASSWORD = "userPassword";
    private final String USERNAME = "uid";
    private final String UIDNUMBER = "uidNumber";
    private final String TYPE = "accountType";
    private final String NAME = "gecos";
    private final String HOME = "homeDirectory";
    private final String COMMONNAME = "cn";
    private final String GIDNUMBER = "gidNumber";
    private final String MEMBERS = "memberUid";

    /**
     * Creates a LDAPSearch with the following credentials
     * @param hostname Hostname of the ldap server to search 
     * @param port Port on which the ldap server listens for connections
     * @param binduser The user used to bind to the ldap server to preform the search
     * @param bindpasswd The password for the user used to bind to the ldap server to preform the search
     * @param searchResults A empty hash map in which to store the search results
     * @param searchType  
     */
    public LDAPSearch(String hostname, int port, String binduser, String bindpasswd, HashMap searchResults, SearchType searchType) {

        this.hostname = hostname;
        this.binduser = binduser;
        this.bindpasswd = bindpasswd;
        this.port = port;
        this.searchResults = searchResults;
        this.searchType = searchType;
        
    }

    /**
     * Defines the base of our search, either we are looking for users or groups.
     */
    public enum SearchType {

        /**
         * Defines a user based search
         */
        USER,
        /**
         * Defines a group based search
         */
        GROUP,
        /**
         * Defines an account group based search
         */
        ACCGROUP,
        
    }

    /**
     * Preforms the actual search query on the ldap server, this will then populate the searchResults hashmap with new
     * user objects
     * @param baseDN The Distinguished Name we should start our query at
     * @param scope The search scope the search should be limited to
     * @param attributes The attributes we are searching for
     * @throws LDAPSearchException  
     */
    private void preformSearch(String baseDN, SearchScope scope, String attributes) throws LDAPSearchException, NullPointerException {
        SearchResult sr = null;

        sr = connection.search(baseDN, scope, attributes);

        if (sr.getEntryCount() > 0) {

            for (SearchResultEntry entry : sr.getSearchEntries()) {

                if (this.searchType == SearchType.USER) {

                    User user = new User();
                    user.setUsername(entry.getAttributeValue(USERNAME));
                    user.setuidNumber(entry.getAttributeValue(UIDNUMBER));
                    user.setPassword(entry.getAttributeValue(PASSWORD));
                    user.setHomeDirectory(entry.getAttributeValue(HOME));
                    user.setFullname(entry.getAttributeValue(NAME));

                    String type = entry.getAttributeValue(TYPE);
                    user.setDN(entry.getDN());

                    if (type == null) {
                        //This should only occur if someone has manually added a user and forget to specify an accounttype attribute

                        Logger.getLogger(LDAPSearch.class.getName()).log(Level.INFO, "Account type has not been set for {0}\n", user.getUsername());
                       
                        user.setAccountType(User.Type.UNKOWN);
                        
                    } else if (type.equals("staff")) {
                        user.setAccountType(User.Type.STAFF);
                    } else if (type.equals("student")) {
                        user.setAccountType(User.Type.STUDENT);
                    } else if (type.equals("associate")){   
                        user.setAccountType(User.Type.ASSOCIATE);   
                    }
                    
                    else {
                        user.setAccountType(User.Type.DEPARTMENT);
                    }


                    searchResults.put(user.getUsername(), user);
                    
                } else if (this.searchType == SearchType.GROUP || this.searchType == SearchType.ACCGROUP) {

                   Group group = new Group();
                   group.setGidNumber(entry.getAttributeValue(GIDNUMBER));
                   group.setGroupName(entry.getAttributeValue(COMMONNAME));
                   
                   //A group may have no members 
                   if(entry.getAttributeValues(MEMBERS) != null ){
                        group.setMembers(entry.getAttributeValues(MEMBERS));
                   } else{
                       group.setMembers(new String[0]);
                       
                   }
                   
                   group.setGroupDN(entry.getDN());
                   
                   searchResults.put(group.getGroupName(), group);

                } 
            }            
        }
    }

    /**
     * The NotificationThread run method, when this thread is scheduled to run this method is executed
     * 
     */
    @Override
    public void doRun() {

        try {

            connection = ldapConnect(hostname, port);

        } catch (LDAPException ex) {
            exceptionOccured(ex, "Could not connect to ldap server");
        } catch (NoSuchAlgorithmException ex) {
            exceptionOccured(ex, "No Such algorithm");
        } catch (KeyStoreException ex) {
            exceptionOccured(ex, "There is a problem with the internal keystore");
        } catch (IOException ex) {
            exceptionOccured(ex);
        } catch (CertificateException ex) {
            exceptionOccured(ex, "Certification failure");
        } catch (GeneralSecurityException ex) {
            exceptionOccured(ex);
        } 

        try {
            String search = "";

            connection = ldapBind(binduser, bindpasswd);
            
            if (searchType == SearchType.USER) {
                search = "ou=users,dc=physics,dc=local";
                preformSearch(search, SearchScope.SUBORDINATE_SUBTREE, "(objectclass=account)");
            } else if (searchType == SearchType.GROUP) {
                search = "ou=usergroups,ou=groups,dc=physics,dc=local";
                preformSearch(search, SearchScope.SUBORDINATE_SUBTREE, "(objectclass=posixGroup)");
            } else if (searchType == SearchType.ACCGROUP){
                search = "ou=accountgroups,ou=groups,dc=physics,dc=local";
                preformSearch(search, SearchScope.SUBORDINATE_SUBTREE, "(objectclass=posixGroup)");
            }

        } catch (LDAPSearchException ex) {
            exceptionOccured(ex, "LDAP Search on base " + searchType);
        } catch (LDAPException ex) {
            exceptionOccured(ex, "Bind to ldap server failed");
        } catch (NullPointerException ex){
            exceptionOccured(ex, "Account or Group is missing expected field");
        }finally {
            
            connection.close();

        }
    }
    /**
     * Ensures any open connections are closed before an exit
     */
    @Override
    public void setshutdownHook() {
         //Shutdown Hook
            Runtime.getRuntime().addShutdownHook(new Thread(){
            @Override
                public void run(){
                    if(connection != null){
                        connection.close();
                    }
                    Logger.getLogger(LDAPSearch.class.getName()).log(Level.FINE, "LDAPSearch shutdown\n");
                }
            });
    }
}
