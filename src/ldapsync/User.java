/*
 * A class to represent a user
 */
package ldapsync;

/**
 * A representation of a user
 * @author sf659
 */
public class User {

    private String username;
    private String fullname;
    private String uidNumber;
    private String password;
    private String homeDirectory;
    private String dn;
    private Type accountType;
    private String accountGroup;

    /**
     * Constructor for user
     */
    public User() {
    }

    /**
     * Enum defining the type of user account 
     */
    public enum Type {

        /**
         * Defines a student account
         */
        STUDENT,
        /**
         * Defines a staff account
         */
        STAFF,
        /**
         * Defines a departmental account
         */
        DEPARTMENT,
        /**
         * Defines an associate account
         */
        ASSOCIATE,
        /**
         * Defines an unknown account
         */
        UNKOWN,
    }

    /**
     * Sets a users username
     * @param username The username to set
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * Sets a users fullname
     * @param fullname The fullname to set
     */
    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    /**
     * Sets a students uid number, note the number should be passed as a string
     * @param uidNumber The uid number to set
     */
    public void setuidNumber(String uidNumber) {
        this.uidNumber = uidNumber;
    }

    /**
     * Sets the users password
     * @param password Password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * Sets the users homedirectory
     * @param homeDirectory Home directory to set
     */
    public void setHomeDirectory(String homeDirectory) {
        this.homeDirectory = homeDirectory;
    }

    /**
     * Sets the account type, should be one of the following<p>
     * User.Type.Student<p>
     * User.Type.Staff<p>
     * User.Type.Departmental<p>
     * User.Type.Unknown<p>
     * @param accountType The account type to be set
     */
    public void setAccountType(Type accountType) {
        this.accountType = accountType;

    }

    /**
     * Set the users distinguished name
     * @param dn the distinguished name to set 
     */
    public void setDN(String dn) {
        this.dn = dn;




    }

    /**
     * Retrieve the users username
     * @return  username 
     */
    public String getUsername() {
        return username;
    }

    /**
     * Retrieve the users password
     * @return password
     */
    public String getPassword() {
        return password;
    }

    /**
     * Retrieve the users distinguished name
     * @return dn
     */
    public String getDN() {
        return dn;
    }

    /**
     * Retrieve the users uid number
     * @return uidNumber
     */
    public String getUidNumber() {
        return uidNumber;
    }

    /**
     * Retrieve the users fullname
     * @return fullname
     */
    public String getFullName() {
        return fullname;
    }

    /**
     * Retrieve the users account type
     * @return accountType
     */
    public Type getAccountTypeEnum() {
        return this.accountType;
    }

    /**
     * Retrieve string representation of the account type
     * @return string representation of account type
     */
    public String getAccountType() {
        switch (accountType) {
            case STUDENT:
                return "student";
            case STAFF:
                return "staff";
            case DEPARTMENT:
                return "department";
            case ASSOCIATE:
                return "associate";
        }

        return "unknown";

    }

     /**
     * Retrieve string representation of the account group 
     * @return string representation of account group
     */
    public String getAccountGroup() {

        switch (accountType) {
            case STUDENT:
                if (dn.matches(".*ou=foundation.*")) {
                    return "foundation";
                } else if (dn.matches(".*ou=firstyear.*")) {
                    return "firstyear";
                } else if (dn.matches(".*ou=secondyear.*")) {
                    return "secondyear";
                } else if (dn.matches(".*ou=thirdyear.*")) {
                    return "thirdyear";
                } else if (dn.matches(".*ou=fourthyear.*")) {
                    return "fourthyear";
                } else if (dn.matches(".*ou=postgrad.*")) {
                    return "postgrad";
                } else {
                    return "unkown";
                }

            case STAFF:
                return "staff";
            case DEPARTMENT:
                return "departmental";
            case ASSOCIATE:
                return "associate";

        }

        return "unknown";

    }

  

    /**
     * Overridden hashCode, we will use the username to generate
     * the hashcode thus two users with the same username are treated as equal
     * @return username.hashcode
     */
    @Override
    public int hashCode() {
        return getUsername().hashCode();

    }

    /**
     * Equality test for two users
     * @param obj Object to test against
     * @return true if userA = userB
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }

        final User other = (User) obj;
        if ((this.username == null) ? (other.username != null) : !this.username.equals(other.username)) {
            return false;
        }

        if (!obj.getClass().isInstance(obj)) {
            System.out.println("Class = " + getClass() + "    obj = " + obj.getClass());

            return false;
        }


        return true;
    }
}
