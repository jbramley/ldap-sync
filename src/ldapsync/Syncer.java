/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ldapsync;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.logging.ConsoleHandler;
import java.util.logging.FileHandler;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;



/**
 * This is where the main setup of the application is started, we read in our properties
 * file, this allows a user to set certain variables and options, once this is complete a new 
 * @{Scheduler} and the syncer is started.
 * @author sf659
 */
public class Syncer {

    private String unix0user;
    private String privatekeyfile;
    private String unix0addr;
    private PropertyReader p;
    private String dwusername;
    private String dwpassword;
    private String dwhost;
    private String dwport;
    private String dwservicename;
    private String ldapserver;
    private int ldapport;
    private String binduser;
    private String binduserpasswd;
    private int scheduleInterval;
    private java.util.logging.Level loggingLevel;
    private String logFile;
    private boolean mailNotifications = false;
    //Start of with some large hash maps over the current student population
    // around 630 in 2012.
    private HashMap ldapUsers = new HashMap(900);
    private HashMap students = new HashMap(800);
    private HashMap ldapGroups = new HashMap(800);

    /**
     * Constructor for Syncer.
     */
    public Syncer() {

            //Shutdown Hook
            Runtime.getRuntime().addShutdownHook(new Thread(){
            @Override
                public void run(){
                    Logger.getLogger(LdapSync.class.getName()).log(Level.INFO, "================= Syncer has been shutdown =================\n");
                        if(Config.mailNotifications){
                            Mailer mail = new Mailer("Syncer shutdown hook initiated - testing", "Syncer has shutdown - testing ");
                        }
                   
                }
                
            });
          
        
        p = new PropertyReader("config.properties");

        retrieveLogLevelProperties();
        setupLogger();
        
        retrieveDatawarehouseProperties();
        retrieveCentralPasswordRetrieverProperties();
        retrieveLDAPOperatorProperties();
        retrieveSchedulerProperties();
        retrieveMailProperties();
        


        LDAPSearch ldapSearch = new LDAPSearch(ldapserver, ldapport, binduser, binduserpasswd, ldapUsers, LDAPSearch.SearchType.USER);
        LDAPSearch ldapSearchGroups = new LDAPSearch(ldapserver, ldapport, binduser, binduserpasswd, ldapGroups, LDAPSearch.SearchType.GROUP);
        LDAPSearch ldapSearchAccGroups = new LDAPSearch(ldapserver, ldapport, binduser, binduserpasswd,ldapGroups, LDAPSearch.SearchType.ACCGROUP);
        
        DatawarehouseReader dwreader = new DatawarehouseReader(dwusername, dwpassword, dwhost, dwport, dwservicename, students);
        
        
        CentralPasswordRetriever cp = new CentralPasswordRetriever(unix0user, unix0addr, privatekeyfile, "sync.file.enc");


        AccountChecker ac = new AccountChecker(students, ldapUsers, ldapGroups, new File("/tmp/sync.file"));



        List scheduleThreads = Arrays.asList(dwreader, cp, ldapSearch, ldapSearchGroups, ldapSearchAccGroups);
        Scheduler sch = new Scheduler(new NotificationThreadGroup(scheduleThreads, ac, false));
        
         

    }

    /**
     * Sets up the FileHandler logger allowing us to effect the detail
     * level of the logs.
     */
    private void setupLogger() {
        try {
            //Setup our loghandler, all exceptions will be logged here, as well 
            // as debug information

            boolean append = true;
            Handler handler = new FileHandler(logFile, append);

            Logger logger = Logger.getLogger("");
            logger.addHandler(handler);


            handler.setFormatter(new SimpleFormatter()); //txt file not xml
            handler.setLevel(loggingLevel);

            setupConsoleHandler(logger);

        } catch (FileNotFoundException ex) {
            Logger.getLogger(LdapSync.class.getName()).log(Level.SEVERE, "Error writting to log file", ex);
            System.exit(-1);
        } catch (IOException ex) {
            Logger.getLogger(Syncer.class.getName()).log(Level.SEVERE,"Please check permissions on file", ex);
            System.exit(-1);
        } catch (SecurityException ex) {
            Logger.getLogger(Syncer.class.getName()).log(Level.SEVERE, null, ex);
            System.exit(-1);
        }

    }
    /**
     * Sets up the ConsoleHandler logger allowing us to effect the detail
     * level of the logs.
     */
    private void setupConsoleHandler(Logger logger) {

        Handler conHander = null;

        for (Handler handles : logger.getHandlers()) {
            if (handles instanceof ConsoleHandler) {

                conHander = handles;
                break;
            }
        }

        conHander.setLevel(loggingLevel);

    }

    /**
     * Retrieves the Dataware house options from config.properties
     */
    private void retrieveDatawarehouseProperties() {
        String currentProperty = "";

        try {
            currentProperty = "dbuser";
            dwusername = p.returnProperty(currentProperty);
            Logger.getLogger(LdapSync.class.getName()).log(Level.INFO, "Connecting to database as: {0}\n", dwusername);

            currentProperty = "dbpassword";
            dwpassword = p.returnProperty(currentProperty);
            Logger.getLogger(LdapSync.class.getName()).log(Level.INFO, "Found password for user: {0}\n", dwusername);

            currentProperty = "dbhost";
            dwhost = p.returnProperty(currentProperty);
            Logger.getLogger(LdapSync.class.getName()).log(Level.INFO, "Connecting to host: {0}\n", dwhost);

            currentProperty = "dbport";
            dwport = p.returnProperty(currentProperty);
            Logger.getLogger(LdapSync.class.getName()).log(Level.INFO, "Connecting to {0} at port: {1}\n", new Object[]{dwhost, dwport});

            currentProperty = "dbservicename";
            dwservicename = p.returnProperty(currentProperty);
            Logger.getLogger(LdapSync.class.getName()).log(Level.INFO, "Service name: {0}\n", dwservicename);
        } catch (NullPointerException ex) {
            Logger.getLogger(LdapSync.class.getName()).log(Level.SEVERE, "Property does not exist: " + currentProperty, ex);
            System.exit(-1);

        }

    }
    /**
     * Retrieves the scheduler options from config.properties
     */
    private void retrieveSchedulerProperties() {
        String currentProperty = "";

        try {
            currentProperty = "schedulerInterval";

            scheduleInterval = Integer.parseInt(p.returnProperty(currentProperty));
            Config.scheduleInterval = scheduleInterval;


        } catch (NullPointerException ex) {
            Logger.getLogger(LdapSync.class.getName()).log(Level.SEVERE, "Property does not exist: " + currentProperty, ex);
            System.exit(-1);

        } catch (NumberFormatException ex) {
            Logger.getLogger(LdapSync.class.getName()).log(Level.SEVERE, "Property is in incorrect format " + currentProperty, ex);
            System.exit(-1);
        }

    }
    /**
     * Retrieves the logger options from config.properties
     */
    private void retrieveLogLevelProperties() {
        String currentProperty = "";
        String level = "";
        try {
            currentProperty = "logLevel";

            level = p.returnProperty(currentProperty);

            loggingLevel = java.util.logging.Level.parse(level);

            currentProperty = "logFile";
            logFile = p.returnProperty(currentProperty);
            

        } catch (NullPointerException ex) {
            Logger.getLogger(LdapSync.class.getName()).log(Level.SEVERE, "Property does not exist: " + currentProperty, ex);
            System.exit(-1);
        } catch (IllegalArgumentException ex) {
            Logger.getLogger(LdapSync.class.getName()).log(Level.SEVERE, "Log level: " + level + " is not valid", ex);
            System.exit(-1);
        }

    }
    /**
     * Retrieves the mailer options from config.properties
     */
    private void retrieveMailProperties(){
        String currentProperty = "";
        try {
            currentProperty = "mailNotifications";

           //Note if mailNotifications is anything but "true" this will be set to false
           mailNotifications = Boolean.valueOf(p.returnProperty(currentProperty));
           Config.mailNotifications = this.mailNotifications;
           
        } catch (NullPointerException ex) {
            Logger.getLogger(LdapSync.class.getName()).log(Level.SEVERE, "Property does not exist: " + currentProperty, ex);
            System.exit(-1);
        } 
        
        
        
    }
    /**
     * Retrieves the unix0 login options from config.properties
     */
    private void retrieveCentralPasswordRetrieverProperties() {
        String currentProperty = "";
        try {
            currentProperty = "unix0user";
            unix0user = p.returnProperty(currentProperty);
            Logger.getLogger(LdapSync.class.getName()).log(Level.INFO, "unix0 user: {0}\n", unix0user);

            currentProperty = "privatekeyfile";
            privatekeyfile = p.returnProperty(currentProperty);
            Logger.getLogger(LdapSync.class.getName()).log(Level.INFO, "Private key file location: {0}\n", privatekeyfile);


            currentProperty = "unix0addr";
            unix0addr = p.returnProperty(currentProperty);
            Logger.getLogger(LdapSync.class.getName()).log(Level.INFO, "Unix0 Address: {0}\n", unix0addr);
        } catch (NullPointerException ex) {
            Logger.getLogger(LdapSync.class.getName()).log(Level.SEVERE, "Property does not exist: " + currentProperty, ex);


        }

    }
    /**
     * Retrieves the ldap server options from config.properties
     */
    private void retrieveLDAPOperatorProperties() {
        String currentProperty = "";
        try {
            currentProperty = "ldapserver";
            ldapserver = p.returnProperty(currentProperty);
            Logger.getLogger(LdapSync.class.getName()).log(Level.INFO, "LDAPServer Address: {0}\n", ldapserver);
            Config.ldapserver = ldapserver;


            currentProperty = "ldapport";
            String lport = p.returnProperty(currentProperty);
            Logger.getLogger(LdapSync.class.getName()).log(Level.INFO, "LDAPServer Port: {0}\n", lport);
            ldapport = Integer.parseInt(lport);
            Config.ldapport = ldapport;


            currentProperty = "binduser";
            binduser = p.returnProperty(currentProperty);
            Logger.getLogger(LdapSync.class.getName()).log(Level.INFO, "Binding to {0} as {1}\n", new Object[]{ldapserver, binduser});
            Config.binduser = binduser;

            currentProperty = "binduserpasswd";
            binduserpasswd = p.returnProperty(currentProperty);
            Logger.getLogger(LdapSync.class.getName()).log(Level.INFO, "Found password for {0}\n", binduser);
            Config.binduserpasswd = binduserpasswd;


        } catch (NullPointerException ex) {
            Logger.getLogger(LdapSync.class.getName()).log(Level.SEVERE, "Property does not exist: " + currentProperty, ex);
        } catch (NumberFormatException ex) {
            Logger.getLogger(LdapSync.class.getName()).log(Level.SEVERE, "LDAP Port number incorrectly defined", ex);

        }

    }
}
