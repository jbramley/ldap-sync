
package ldapsync;

import com.unboundid.ldap.sdk.LDAPException;
import com.unboundid.ldap.sdk.ResultCode;
import java.util.ArrayList;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * The ScheduleService class is responsible for scheduling and executing the NotificationThreads in the 
 * correct order.
 * @author sf659
 */
public class ScheduleServices implements TaskListener {

    private ScheduledExecutorService scheduler;
    private long delayBetweenEvents;
    private TimeUnit unit;
    private final long DELAY_TO_START = 10;
    private ArrayList<NotificationThreadGroup> notifyGroupThreadTracking;
    private final static int MAXIMUM_JOBS_SCHEDULED = 7; // This is the maximum number of jobs we will have scheduled at anytime

    /**
     * Constuctor for a ScheduleService object, we pass in an array of NotificationThreads defined
     * as taskstobeScheduled, these are a list of threads that can run concurrently (i.e do not have any deadlock, competing
     * resource issues), we also pass a NotificationThread called complete this is a thread that requires that all the NotificationThreads 
     * inside taskstobeScheduled have completed before it is scheduled to run.
     * @param delayBetweenEvents The delay between running of the scheduled NotificationThread
     * @param unit The unit of time for the delay 
     */
    public ScheduleServices(long delayBetweenEvents, TimeUnit unit) {

        this.delayBetweenEvents = delayBetweenEvents;
        this.unit = unit;
        notifyGroupThreadTracking = new ArrayList<NotificationThreadGroup>();

        scheduler = new ScheduledThreadPoolExecutor(MAXIMUM_JOBS_SCHEDULED);
    }

    /**
     * Schedules all the threads inside taskstobeScheduled to run at the desired delay, we create a scheduledThreadPool that is 
     * one larger than the taskstobeScheduled array to allow for the final NotificationThread (complete) to be run
     */
    @Override
    public void scheduleEvents(NotificationThreadGroup notificationGroup) {

        notificationGroup.addNotificationThreadListener(this);
        notifyGroupThreadTracking.add(notificationGroup);

        if (notificationGroup.isOneTimeRun()) {
            //schedule but with no repeat
            for (NotificationThread o : notificationGroup.returnNotificationThreadGroup()) {
                scheduler.execute(o);
            }
        } else {
            //schedule with repeat
            for (NotificationThread o : notificationGroup.returnNotificationThreadGroup()) {
                scheduler.scheduleWithFixedDelay(o, DELAY_TO_START, delayBetweenEvents, unit);
            }

            //CODE TO BE REMOVED IN PRODUCTION, Just prints out the count every 1 seconds
           // int countLoop = 0;
            //while (!scheduler.isShutdown()) {
               // try {

                 //   countLoop = countLoop + 1;
                 //   Thread.sleep(1000);
                //    System.out.println("Running: " + countLoop);
               // } catch (InterruptedException ex) {
              //      Logger.getLogger(Scheduler.class.getName()).log(Level.SEVERE, null, ex);
               // }
            //}

        }

    }

    /**
     * Once all of the taskstobeScheduled have completed we finally schedule our complete NotificationThread
     * to be run.
     * @param runner A NotificationThread that has this object assigned as a listener
     */
    @Override
    public void threadComplete(NotificationThread runner) {
       
        for (NotificationThreadGroup group : notifyGroupThreadTracking) {

            if (group.containsNotificationThread(runner)) {
                group.threadComplete(runner);

            }
            if (group.allComplete()) {
                //Only try and schedule the follow on job if it exists.
                if(group.doesrunCompleteExists()){
                    scheduler.execute(group.returnRunCompleteThread());
                }
            }
        }
    }

    /**
     * Add this listener to a {@link NotificationThread}
     * @param nt {@link NotificationThread} to add this listner to
     */
    private void addListener(NotificationThread nt) {
        if (nt instanceof NotificationThread) {
            nt.addListener(this);
        } else {
            throw new ClassCastException();
        }

    }

    /**
     * All exceptiontions in NotificationThreads are passed here for handling and logging 
     * @param runner The {@link NotificationThread} the exception occured in
     * @param ex The exception that occured
     */
    @Override
    public void exceptionOccured(NotificationThread runner, Exception ex) {

        scheduler.shutdown();
        Logger.getLogger(CentralPasswordRetriever.class.getName()).log(Level.SEVERE, "Error Occured in " + runner.getClass().toString() + " Scheduler: SHUTDOWN", ex);
        if (Config.mailNotifications) {
            Mailer mail = new Mailer(ex, "LDAP SYNCER: Error Occured in " + runner.getClass().toString() + " Scheduler: SHUTDOWN");

        }
    }

    /**
     * All exceptiontions in NotificationThreads are passed here for handling and logging 
     * @param runner The {@link NotificationThread} the exception occured in
     * @param ex The exception that occured
     * @param msg A custom message 
     */
    @Override
    public void exceptionOccured(NotificationThread runner, Exception ex, String msg) {

        scheduler.shutdown();
        Logger.getLogger(CentralPasswordRetriever.class.getName()).log(Level.SEVERE, "Error Occured in " + runner.getClass().toString() + " " + msg + " Scheduler: SHUTDOWN", ex);
        if (Config.mailNotifications) {
            Mailer mail = new Mailer(ex, "LDAP SYNCER: Error Occured in " + runner.getClass().toString() + " Scheduler: SHUTDOWN");
        }
    }

    /**
     * All exceptiontions in NotificationThreads are passed here for handling and logging,
     * this is classed as a non fatal exception by the {@link NotificationThread} it 
     * will be up the the ScheduleServices to determine the outcome.
     * @param runner The {@link NotificationThread} the exception occured in
     * @param ex The exception that occured
     */
    @Override
    public void nonFatalExceptionOccured(NotificationThread runner, Exception ex) {
        //to Start with treat this as a non fatal issue i.e dont stop the scheduler
        if (LDAPException.class.isInstance(ex)) {

            LDAPException exception = (LDAPException) ex;

            ResultCode resultCode = exception.getResultCode();

            switch (resultCode.intValue()) {
                case ResultCode.INSUFFICIENT_ACCESS_RIGHTS_INT_VALUE:
                    //This will be fatal, the bind user is unable to modify the ldap entries
                    // due to insufficient access rights
                    scheduler.shutdown();
                    Logger.getLogger(CentralPasswordRetriever.class.getName()).log(Level.SEVERE, 
                            "Error Occured in " + runner.getClass().toString() + " Scheduler: SHUTDOWN", ex);
                    
                    if (Config.mailNotifications) {
                        Mailer mail = new Mailer(ex, "LDAP SYNCER: Error Occured in " + runner.getClass().toString() + " Scheduler: SHUTDOWN");
                    }
                    break;
                default:
                    //In all other cases at the moment we shall just log the error and continue
                    Logger.getLogger(CentralPasswordRetriever.class.getName()).log(Level.SEVERE, "Error Occured in " + runner.getClass().toString(), ex);
                    break;

            }

        }

    }
}
