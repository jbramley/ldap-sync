
package ldapsync;


import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.daemon.Daemon;
import org.apache.commons.daemon.DaemonContext;
import org.apache.commons.daemon.DaemonInitException;


/**
 *
 * @author sf659
 */
public class LdapSync implements Daemon{

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args)  {
       Syncer sync = new Syncer();

    }

    /**
     * Initialise daemon
     * @param dc
     * @throws DaemonInitException
     * @throws Exception
     */
    @Override
    public void init(DaemonContext dc) throws DaemonInitException, Exception {
     
    }

    /**
     * Start daemon
     * @throws Exception
     */
    @Override
    public void start() throws Exception {
         Logger.getLogger(LdapSync.class.getName()).log(Level.INFO, "========= Password Sync Started ========");
         main(null);
    }

    /**
     * Stop daemon
     * @throws Exception
     */
    @Override
    public void stop() throws Exception {
        if (Config.mailNotifications){
            Mailer mail = new Mailer("LDAP Syncer has been stopped","SHUTDOWN NOTICE: LDAP SYNC");
        }
         System.exit(0);
    }

    /**
     * Destroy daemon
     */
    @Override
    public void destroy() {
       
    }
}
