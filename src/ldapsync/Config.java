package ldapsync;

/**
 *
 * @author sf659
 */
public class Config {
    
    /**
     * global ldapserver
     */
    public static String ldapserver;
   /**
    * global ldapport 
    */
   public static int ldapport;
   /**
    * global binduser
    */
   public static String binduser;
   /**
    * global binduserpassword
    */
   public static String binduserpasswd;
   /**
    * global schedule interval, how often the syncer runs
    */
   public static int scheduleInterval;
    /**
    * global mailNotifications, defines if mail notifications should be sent
    */
   public static boolean mailNotifications;
    
}
