package ldapsync;

import ch.ethz.ssh2.ChannelCondition;
import ch.ethz.ssh2.Connection;
import ch.ethz.ssh2.SCPClient;
import ch.ethz.ssh2.Session;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.GeneralSecurityException;
import java.security.SecureRandom;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * The CentralPasswordRetriever class is responsible for retrieving the centralised password information for all 
 * current and future users. It does this by starting an SSH session currently to unix0</p>
 * 
 * <b>Details: http://www.york.ac.uk/it-services/it/unix/</b></p>
 * 
 * Once a valid ssh session has been created we then retrieve a list of usernames and password hashses by running the command
 * <b>ypcat passwd > output</b> the output file is then copied to the local host so we can extract the required password
 * hashes.
 * 
 * 
 * @author sf659
 */
public class CentralPasswordRetriever extends NotificationThread {

    /** user to connect to ssh server */
    String user;
    /** ssh server to connect to, in this case this should be unix0 */
    String host;
    /** the hostkey of the ssh server */
    String hostkey;
    /** the users ssh key required to connect to the server */
    String userPrivateKey;
    /** where to write the password file locally */
    String outputFile;
    Connection sshClient = null;
    Session session = null;
    Random random;
    private String encryptPasswd;

    /**
     * Creates a new CentralPasswordRetriever with the following credentials.
     * @param user Username used to connect to host
     * @param host The server to connect to (this should be unix0)
     * @param userPrivateKey The private key location used for passwordless login to host
     * @param outputFile The full path of the output file to generate
     */
    public CentralPasswordRetriever(String user, String host, String userPrivateKey, String outputFile) {

        this.user = user;
        this.host = host;
        this.userPrivateKey = userPrivateKey;
        this.outputFile = outputFile;
    }

    /**
     * Connects and authenticates to the ssh server defined with the host parameter
     * @throws IOException 
     */
    private void connect() throws IOException {

        sshClient = new Connection(host);
        sshClient.connect();
        sshClient.authenticateWithPublicKey(user, new File(userPrivateKey), null);

    }

    /**
     * Creates a ssh session, generates the output file and copies it back to the localhost,
     * 
     * @throws IOException 
     */
    private void newSession() throws IOException, GeneralSecurityException {
        connect();

        random = new SecureRandom();
        //Generate new password each time
        encryptPasswd = nextRandomString();

        session = sshClient.openSession();
        Logger.getLogger(CentralPasswordRetriever.class.getName()).log(Level.INFO, "SSH Client Connected Sucessfully\n");

        //Need to make sure the aescrypt binary is available 
        //http://www.aescrypt.com/linux_aes_crypt.html
        session.execCommand("ypcat passwd | aescrypt_bin/aescrypt -e -p " + encryptPasswd + " - > sync.file.enc");

        //wait for the command to finish
        Logger.getLogger(CentralPasswordRetriever.class.getName()).log(Level.INFO, "Creating password file\n");

        session.waitForCondition(ChannelCondition.STDOUT_DATA, 0);

        SCPClient scp = new SCPClient(sshClient);
        Logger.getLogger(CentralPasswordRetriever.class.getName()).log(Level.INFO, "Copying password file to local file system\n");
        scp.get("sync.file.enc", "/tmp/");

        Logger.getLogger(CentralPasswordRetriever.class.getName()).log(Level.INFO, "Sucessfully copied password file to local file system\n");

        decrypt();
        cleanUp();


    }

    /**
     * Cleans up the ssh session, this will close down the client and the current open session
     * aswell as removing the temporary file generated on the host server
     * @throws IOException 
     */
    private void cleanUp() throws IOException {

        Logger.getLogger(CentralPasswordRetriever.class.getName()).log(Level.INFO, "Attemping clean up of ssh client\n");
        session.close(); //close previous session
        session = sshClient.openSession(); //open new session to remove file
        Logger.getLogger(CentralPasswordRetriever.class.getName()).log(Level.INFO, "Removing password file on remote system\n");
        session.execCommand("rm sync.file.enc");


        session.close();
        sshClient.close();

        session = null;
        sshClient = null;
        encryptPasswd = null;

    }

    private void decrypt() throws GeneralSecurityException, UnsupportedEncodingException, IOException {

        AESCrypt decrypter = new AESCrypt(encryptPasswd);
        decrypter.decrypt("/tmp/sync.file.enc", "/tmp/sync.file");

    }

    /**
     * The NotificationThread run method, when this thread is scheduled to run this method is executed
     */
    @Override
    public void doRun() {
        try {

            newSession();
        } catch (GeneralSecurityException ex) {
            exceptionOccured(ex);
        } catch (IOException ex) {
            exceptionOccured(ex);
        } finally {
            if (session != null) {
                session.close();
            }
            if (sshClient != null) {
                session.close();
            }

        }
    }

    /**
     * Ensures any open connections are closed before an exit
     */
    @Override
    public void setshutdownHook() {
        //Shutdown Hook
        Runtime.getRuntime().addShutdownHook(new Thread() {

            @Override
            public void run() {
                // Sessions may already be closed as we are unsure when the 
                // interupt / exit will happen
                if (session != null) {
                    session.close();
                }
                if (sshClient != null) {
                    sshClient.close();
                }
                Logger.getLogger(CentralPasswordRetriever.class.getName()).log(Level.FINE, "CentralPasswordRetiever Shutdown \n");
            }
        });

    }

    private String nextRandomString() {
        return new BigInteger(130, random).toString(32);
    }
}
