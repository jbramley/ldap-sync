
package ldapsync;



import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * Scheduler is responsible for setting up the {@link ScheduleServices} which will 
 * actualy schedule the {@link NotificationThread}s
 * @author sf659
 */
public class Scheduler {
    
  
    private NotificationThreadGroup scheduleGroup;
    
    ScheduledExecutorService scheduler;
    
    /**
     * Constructor for scheduler
     * @param scheduleGroup A {@link  NotificationThreadGroup} to be scheduled
     */
    public Scheduler(NotificationThreadGroup scheduleGroup){
        this.scheduleGroup = scheduleGroup;
        scheduleCheck();
        
    }
    
    private void scheduleCheck(){
        
        //create a new scheduleservices
        ScheduleServices scheduleServices = 
                new ScheduleServices(Config.scheduleInterval, TimeUnit.SECONDS);

        scheduleServices.scheduleEvents(scheduleGroup);
    
    }
    
   
}
